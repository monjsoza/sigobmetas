﻿using System.Collections.Generic;
using SkiaSharp;
using Entry = Microcharts.Entry;

namespace MetasSigob.Microcharts
{

    public class Microcharts_Data
    {
        public List<Entry> GetEnGestion()
        {
            List<Entry> data = new List<Entry>
            {
                new Entry(58)
                {
                    //ValueLabel ="58",
                    Color = SKColor.Parse("#019D08"),
                    //TextColor = SKColor.Parse("#019D08"),
                },
            };
            return data;
        }
        public List<Entry> GetAtrasadas()
        {
            List<Entry> data = new List<Entry>
            {
                new Entry(58)
                {
                    //ValueLabel ="58",
                    Color = SKColor.Parse("#ecf708"),
                    //TextColor = SKColor.Parse("#019D08"),
                    
                },
            };
            return data;
        }
        public List<Entry> GetDetenidas()
        {
            List<Entry> data = new List<Entry>
            {
                new Entry(58)
                {
                    //ValueLabel ="58",
                    Color = SKColor.Parse("#f70808"),
                    //TextColor = SKColor.Parse("#019D08"),
                },
            };
            return data;
        }
        public List<Entry> GetTerminadas()
        {
            List<Entry> data = new List<Entry>
            {
                new Entry(58)
                {
                    //ValueLabel ="58",
                    Color = SKColor.Parse("#0070ff"),
                    //TextColor = SKColor.Parse("#019D08"),
                },
            };
            return data;
        }
        public List<Entry> GetBarras()
        {
            List<Entry> data = new List<Entry>
            {
               new Entry(200)
            {
                Label = "Enero",
                ValueLabel = "200",
                Color = SKColor.Parse("#266489")
            },
            new Entry(250)
            {
                Label = "Febrero",
                ValueLabel = "250",
                Color = SKColor.Parse("#68B9C0")
            },
            new Entry(100)
            {
                Label = "Marzo",
                ValueLabel = "100",
                Color = SKColor.Parse("#90D585")
            },
            new Entry(150)
            {
                Label = "Abril",
                ValueLabel = "150",
                Color = SKColor.Parse("#e77e23")
}
            };
            return data;
        }

        public List<Entry> GetLineas()
        {
            List<Entry> data = new List<Entry>
            {
               new Entry(200)
            {
                Label = "Enero",
                ValueLabel = "200",
                Color = SKColor.Parse("#266489")
            },
            new Entry(250)
            {
                Label = "Febrero",
                ValueLabel = "250",
                Color = SKColor.Parse("#68B9C0")
            },
            new Entry(100)
            {
                Label = "Marzo",
                ValueLabel = "100",
                Color = SKColor.Parse("#90D585")
            },
            new Entry(150)
            {
                Label = "Abril",
                ValueLabel = "150",
                Color = SKColor.Parse("#e77e23")
}
            };
            return data;
        }
    }
}
