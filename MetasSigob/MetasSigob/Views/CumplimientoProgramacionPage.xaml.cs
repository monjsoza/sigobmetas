﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using MetasSigob.Microcharts;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MetasSigob.Views;

namespace MetasSigob.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CumplimientoProgramacionPage : ContentPage
	{
        Microcharts_Data data = new Microcharts_Data();
        public CumplimientoProgramacionPage()
		{
			InitializeComponent ();
            Title = "Cumplimiento de la Programación";
            Lineas.Chart = new LineChart() { Entries = data.GetLineas() };
        }
	}
}