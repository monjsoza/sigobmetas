﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetasSigob.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MetasSigob.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EventosPage : ContentPage
	{
		public EventosPage ()
		{
			InitializeComponent ();
		}

 

        private void OnCumplimientoClicked(object sender, EventArgs e)
        {
            //NavigationPage nav = new NavigationPage(new CumplimientoProgramacionPage());
            this.Navigation.PushAsync(new CumplimientoProgramacionPage());
        }

        private void OnAtrasadasClicked(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new ListAtrasadasPage());
        }

        private void OnAlertasRestriccionesClicked(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new ListAlertasRestriccionesPage());
        }
        private void OnReprogramadasClicked(object sender, EventArgs e)
        {
            //this.Navigation.PushAsync(new ListAlertasRestriccionesPage());
        }
    }
}