﻿using Microcharts;
using MetasSigob.Microcharts;
using MetasSigob.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MetasSigob.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GestionPage : ContentPage
	{
        
        Microcharts_Data data = new Microcharts_Data();
        public GestionPage ()
		{
			InitializeComponent ();
            Title = "Gestión";
            EnGestion.Chart = new RadialGaugeChart { Entries = data.GetEnGestion() };
            Atrasadas.Chart = new RadialGaugeChart { Entries = data.GetAtrasadas() };
            Detenidas.Chart = new RadialGaugeChart { Entries = data.GetDetenidas() };
            Terminadas.Chart = new RadialGaugeChart { Entries = data.GetTerminadas() };
        }

      
    }
}