﻿using Microcharts;
using Microcharts.Forms;
using MetasSigob.Microcharts;
using MetasSigob.Views;
using MetasSigob.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;

namespace MetasSigob.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OportunidadesPage : TabbedPage
    {
        Microcharts_Data data = new Microcharts_Data();
        public OportunidadesPage ()
        {
            InitializeComponent();
            Title = "Oportunidades";
            Barras.Chart = new BarChart() { Entries = data.GetBarras() };

            //MapView.MapType = MapType.Street;
            //MapView.MoveToRegion(
            //    MapSpan.FromCenterAndRadius(
            //    new Position(4.0000000, -72.0000000),
            //    Distance.FromMiles(1)));


        }
    }
}